const users = require('../1-users.cjs')

const Python = {
    users: {}
};
const Javascript = {
    users: {}
};
const Golang = {
    users: {}
};

const group = () => {
    for (const key in users) {
        if (users[key].designation.includes("Python"))
            Python.users[key] = users[key];

        if (users[key].designation.includes("Javascript"))
            Javascript.users[key] = users[key];

        if (users[key].designation.includes("Golang"))
            Golang.users[key] = users[key];
    }

    console.log("Python Group Members are : \n", Python.users);
    console.log("JavaScript Group Members are : \n", Javascript.users);
    console.log("Golang Group Members are : \n", Golang.users);
}

group();

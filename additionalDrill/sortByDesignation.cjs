const users = require('../1-users.cjs')

const findDesignation = () => {                 //function to pull out only the designation of the users

    let designationArray = [];

    for (const key in users) {

        designationArray.push(users[key].designation)
    }

    return designationArray;
}

const sortDesignation = (designationArray) => {         //function to sort the designation on the basis of assigned value

    let arrLength = designationArray.length
    console.log(arrLength)

    for (index1 = 0; index1 < arrLength - 1; index1++) {

        let value1 = assignValue(designationArray[index1])

        for (index2 = index1 + 1; index2 < arrLength; index2++) {

            let value2 = assignValue(designationArray[index2])

            if (value2 > value1) {
                let swap = designationArray[index1]
                designationArray[index1] = designationArray[index2]
                designationArray[index2] = swap

                // Update value1 after swapping
                value1 = value2;
            }

        }
    }

    return designationArray;

}

function assignValue(designation) {         //function to assign precedence to the designation

    if (designation.includes("Senior")) {
        //console.log("Senior")
        return 2;
    }
    else if (designation.includes("Developer")) {
        //console.log("Developer")
        return 1;
    }
    else {
        //console.log("Intern")
        return 0
    }

}

function sortedObject(sortedArray) {
    sortedArray.forEach(element => {
        for (const key in users) {
            if (users[key].designation === element) {
                console.log(users[key])
                break;
            }
        }
    });
}

const designationArray = findDesignation();
const sortedArray = sortDesignation(designationArray);
sortedObject(sortedArray)
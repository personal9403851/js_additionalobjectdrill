const users = require('../1-users.cjs')

const result = () => {

    let videoGamePlayers = [];
    let checkFlag = "Video Games";

    for (const player in users) {

        let interestOfPlayers = users[player].interests[0]
        if (interestOfPlayers.includes(checkFlag))
        {
            videoGamePlayers.push(player)
        }
    }
    return videoGamePlayers
}

console.log(result())